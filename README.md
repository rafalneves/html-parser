Pretende-se que seja implementada uma CLI (Command Line Interface) em Node.js que
receba como input um URL para um recurso HTML remoto e que devolva como output
um JSON com meta informação relevante sobre a markup obtida.

Exemplos de meta informação pertinente:

* Contagem de tags HTML (por nome);
* Contagem de atributos por tag HTML;
* Tipos de recursos descarregados (imagem, vídeo, etc) e hosts de onde foram
  descarregados;
* Número de filhos e quais, por tipo de tag HTML;
* A profundidade da árvore;

##Instruções
git clone https://rafalneves@bitbucket.org/rafalneves/html-parser.git

cd html-parser

npm install

node html-parser.js url mode[o]

* node html-parser.js https://mixingpixels.com

* node html-parser.js https://mixingpixels.com debug


###Correr exemplo:

npm start

###Correr testes:

npm test
