var args = process.argv.slice(2);

if(args.length){
	var winston = require("winston"),
		validUrl = require("valid-url");

	var logger = new (winston.Logger)({
		level: args[1] && args[1] == "debug" ? "debug" : "info",
		transports: [new (winston.transports.Console)({ "timestamp": true })]
	});

	logger.debug("Validating URL");
	if(validUrl.isUri(args[0])){
		var request = require("request");

		logger.debug("Requesting URL");
		request({ url: args[0], followAllRedirects: true }, function(error, response, body){
			if(error)
				console.log({ error: true, message: error });
			else
				if(response.statusCode == 200){
					logger.debug("Calling parser");

					var Parser = require("./parser.js");

					var data = new Parser(args[0], body).parse().then(data => {
						console.log(JSON.stringify({ error: false, data: data }));
					}).catch(error => {
						console.log(JSON.stringify({ error: true, message: error }));
					});
				}else
					console.log(JSON.stringify({ error: true, message: "Getting wrong status code: " + response.statusCode }));
		});
	}else
		console.log(JSON.stringify({ error: true, message: "Invalid URL!" }));
}else
	console.log(JSON.stringify({ error: true, message: "Missing arguments!" }));