module.exports = class Parser {
	constructor(url, body){
		this.url = url;
		this.hostname = require("url").parse(this.url).hostname;
		this.body = body;

		this.depth = function(element){
			var d = 0;

			function getElementDepth(element, l){
				if(element.children)
					for(var i = 0; i < element.children.length; i++){
						var levelD = getElementDepth(element.children[i], l + 1);

						if(levelD > d)
							d = levelD;
					}

				return l;
			}

			getElementDepth(element, 0);

			return d;
		}

		this.checkIfThere = function(list, value){
			if(list)
				for(var i = 0; i < list.length; i++)
					if(list[i].value == value)
						return list[i];
			return null;
		}
	}
	parse(){
		//not async but using promise anyway
		return new Promise((resolve, reject) => {
			if(this.body){
				var jsdom = require("jsdom").jsdom,
					validUrl = require("valid-url"),
					url = require("url"),
					mime = require("mime");

				let document = jsdom(this.body), data = {
					tag_count: document.getElementsByTagName('*').length,
					tag_list: new Array(),
					resources: new Array(),
					depth: this.depth(document)
				};

				Array.prototype.slice.call(document.getElementsByTagName('*')).forEach(item => {
					/*
						Could download and check size or get type from file magic: https://en.wikipedia.org/wiki/List_of_file_signatures
						With an array of promisses (npm - async and request) - not sure if it's what's requested, going simple:
					*/
					for(var i = 0; i < item.attributes.length; i++)
						if(
							(
								item.attributes[i].nodeName == "src"
								|| (
									item.attributes[i].nodeName == "href" && item.tagName != 'a'
								)
							)
							&& item.attributes[i].nodeValue
							&& validUrl.isUri(
								item.attributes[i].nodeValue.indexOf("http") != -1
								? item.attributes[i].nodeValue
								: this.url.split(this.hostname)[0] + this.hostname + item.attributes[i].nodeValue
							)
						)
							if(!this.checkIfThere(data.resources, item.attributes[i].nodeValue))
								data.resources.push({
									value: item.attributes[i].nodeValue,
									type: mime.lookup(item.attributes[i].nodeValue),
									host: url.parse(item.attributes[i].nodeValue).hostname || this.hostname
								});

					var listItem = this.checkIfThere(data.tag_list, item.tagName);

					var children = listItem ? listItem.list_children : new Array();

					for(var i = 0; i < item.children.length; i++){
						var listChildren = this.checkIfThere(children, item.children[i].tagName);

						if(!listChildren)
							children.push({
								value: item.children[i].tagName,
								count: 1
							});
						else
							++listChildren.count;
					}

					if(!listItem)
						data.tag_list.push({
							value: item.tagName,
							count: 1,
							count_attributes: item.attributes.length,
							count_children: item.childElementCount,
							list_children: children
						});
					else{
						++listItem.count;
						listItem.count_attributes += item.attributes.length;
						listItem.count_children += item.childElementCount;
						listItem.list_children = children;
					}
				});

				return resolve(data)
			}else
				return resolve({});
		});
	}
}