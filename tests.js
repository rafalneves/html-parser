var expect = require("chai").expect;
var spawn = require("child_process").spawn;

describe("No parameters", function(){
	it("Returns a valid JSON", function(done){
		call(["html-parser.js"], (code, data) => {
			expect(/^[\],:{}\s]*$/.test(data.replace(/\\["\\\/bfnrtu]/g, '@').
				replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
				replace(/(?:^|:|,)(?:\s*\[)+/g, ''))).to.equal(true);
			done();
		});
	});
	it("Returns error", function(done){
		call(["html-parser.js"], (code, data) => {
			expect(JSON.parse(data).error).to.equal(true);
			done();
		});
	});
	it("Returns error message Missing arguments!", function(done){
		call(["html-parser.js"], (code, data) => {
			expect(JSON.parse(data).message).to.equal("Missing arguments!");
			done();
		});
	});
});
describe("Invalid URL", function(){
	it("Returns a valid JSON", function(done){
		call(["html-parser.js", "aaaaaa"], (code, data) => {
			expect(/^[\],:{}\s]*$/.test(data.replace(/\\["\\\/bfnrtu]/g, '@').
				replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
				replace(/(?:^|:|,)(?:\s*\[)+/g, ''))).to.equal(true);
			done();
		});
	});
	it("Returns error", function(done){
		call(["html-parser.js", "aaaaaa"], (code, data) => {
			expect(JSON.parse(data).error).to.equal(true);
			done();
		});
	});
	it("Returns error message Invalid URL!", function(done){
		call(["html-parser.js", "aaaaaa"], (code, data) => {
			expect(JSON.parse(data).message).to.equal("Invalid URL!");
			done();
		});
	});
});
describe("Invalid response", function(){
	it("Returns a valid JSON", function(done){
		call(["html-parser.js", "https://mixingpixels.com/aaa/aaa.txt"], (code, data) => {
			expect(/^[\],:{}\s]*$/.test(data.replace(/\\["\\\/bfnrtu]/g, '@').
				replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
				replace(/(?:^|:|,)(?:\s*\[)+/g, ''))).to.equal(true);
			done();
		});
	});
	it("Returns error", function(done){
		call(["html-parser.js", "https://mixingpixels.com/aaa/aaa.txt"], (code, data) => {
			expect(JSON.parse(data).error).to.equal(true);
			done();
		});
	});
	it("Returns error message Getting wrong status code!", function(done){
		call(["html-parser.js", "https://mixingpixels.com/aaa/aaa.txt"], (code, data) => {
			expect(JSON.parse(data).message).to.equal("Getting wrong status code: 404");
			done();
		});
	});
});
describe("Parse MixingPixels.com", function(){
	it("Returns a valid JSON", function(done){
		call(["html-parser.js", "https://mixingpixels.com"], (code, data) => {
			expect(/^[\],:{}\s]*$/.test(data.replace(/\\["\\\/bfnrtu]/g, '@').
				replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
				replace(/(?:^|:|,)(?:\s*\[)+/g, ''))).to.equal(true);
			done();
		});
	});
	it("Returns success", function(done){
		call(["html-parser.js", "https://mixingpixels.com"], (code, data) => {
			expect(JSON.parse(data).error).to.equal(false);
			done();
		});
	});
	it("Returns matching values - Tag List", function(done){
		call(["html-parser.js", "https://mixingpixels.com"], (code, data) => {
			expect(JSON.parse(data).data.tag_list.length).to.equal(8);
			done();
		});
	});
	it("Returns matching values - Tag Count", function(done){
		call(["html-parser.js", "https://mixingpixels.com"], (code, data) => {
			expect(JSON.parse(data).data.tag_count).to.equal(14);
			done();
		});
	});
	it("Returns matching values - Resources", function(done){
		call(["html-parser.js", "https://mixingpixels.com"], (code, data) => {
			expect(JSON.parse(data).data.resources.length).to.equal(5);
			done();
		});
	});
	it("Returns matching values - Depth", function(done){
		call(["html-parser.js", "https://mixingpixels.com"], (code, data) => {
			expect(JSON.parse(data).data.depth).to.equal(3);
			done();
		});
	});
});

function call(params, callback){
	var launch = spawn("node", params),
		buffer = new Buffer(0);
	launch.stdout.on("data", function(chunk){
		buffer = Buffer.concat([buffer, chunk]);
	});
	launch.on("close", function(code){
		callback(code, buffer.toString());
	});
}